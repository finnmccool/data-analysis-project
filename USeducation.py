#!/usr/bin/env python
# coding: utf-8

#The project will use data from US Department of Agriculture, who rather oddly hold a data file on US educational achievment by State and #County covering the period 1970 - 2017. You can either open the data directly as I did or use a local copy.

import pandas as pd
import numpy as np

#The data file is read in from the URL
URL = 'https://www.ers.usda.gov/webdocs/DataFiles/48747/Education.xls'
df = pd.read_excel(URL,header=4)
pd.set_option('display.float_format','{:,.1f}'.format)

#There are a few obvious problems. Every number has been read as a float barring the FIPS code which is an integer.
# The column names are just too unwieldy and there are 47 of them.<br>There is no need to have columns with percentages since these can be #calculated from the data. That should cut down the number of columns substantially. It's fairly easy to remove them.<br>Firstly, create a #list of all the column names from the DF which start with the letter P. Loop through the list and drop all the columns.


#Create a list of all the columns with 'Percent' at the beginning and drop them from DF
dfcols = [i for i in df.columns if i[0] == 'P']
for i in range(0,len(dfcols)):
    df.drop(dfcols[i],axis = 1, inplace = True)
dfcols = [i for i in df.columns]

#That's nicely reduced the number of columns to 27. However, the column names need simplified.<br>There are 4 columns holding codes which #can be simplified to integers.<br>The columns are 3, 4, 5, 6. This uses integer values of the column location to select them and change the #data type to integer.<br>Note how the coulmns are refernced for the pandas iloc command. 
#df.iloc[:,3:7]
#All that #means is [ all rows : , columns 3,4,5,6 ]. First change some of the NaNs to '0' to match the other integer codes.

df.fillna(0,inplace=True)
df.iloc[:,3:7] = df.iloc[:,3:7].astype('int32')
df.head(3)

newdfcols = {'FIPS Code': 'FIPS',
 'State': 'State',
 'Area name': 'Area',
 '2003 Rural-urban Continuum Code': 'rucc_2003',
 '2003 Urban Influence Code': 'uic_2003',
 '2013 Rural-urban Continuum Code': 'rucc_2013',
 '2013 Urban Influence Code': 'uic_2013', 
 'Less than a high school diploma, 1970': 'nhsd_1970',
 'High school diploma only, 1970': 'hsd_1970',
 'Some college (1-3 years), 1970': 'sc13_1970',
 'Four years of college or higher, 1970': '4higher_1970', 
 'Less than a high school diploma, 1980': 'nhsd_1980',
 'High school diploma only, 1980': 'hsd_1980',
 'Some college (1-3 years), 1980': 'sc13_1980',
 'Four years of college or higher, 1980': '4higher_1980', 
 'Less than a high school diploma, 1990':'nhsd_1990' ,
 'High school diploma only, 1990': 'hsd_1990',
 "Some college or associate's degree, 1990": 'sc13_1990',
 "Bachelor's degree or higher, 1990": 'bdhigher_1990', 
 'Less than a high school diploma, 2000': 'nhsd_1990' ,
 'High school diploma only, 2000': 'hsd_2000',
 "Some college or associate's degree, 2000": 'sca_2000',
 "Bachelor's degree or higher, 2000": 'bdhigher_2000',
 'Less than a high school diploma, 2013-17': 'nhsd_201317',
 'High school diploma only, 2013-17': 'hsd_201317',
 "Some college or associate's degree, 2013-17": 'sca_201317',
 "Bachelor's degree or higher, 2013-17": 'bdhigher_201317' }

df = df.rename(newdfcols, axis=1)

URL2 = 'https://www.ers.usda.gov/webdocs/DataFiles/53251/ruralurbancodes2003.xls'
dfcodes2003 = pd.read_excel(URL2)
dfcodes2003['2000 Population '] =  pd.to_numeric(dfcodes['2000 Population '],errors='coerce')
dfcodes2003.head(3)
rucc2003 = pd.DataFrame({'codes':dfcodes2003['2003 Rural-urban Continuum Code'].unique(),
                         'description':dfcodes2003['Description for 2003 codes'].unique()})
rucc2003.sort_values(by=['codes'],inplace=True)
rucc2003.reset_index(drop=True,inplace=True)
rucc2003




